# Righteous Regular Font

Righteous Regular Font used in conjunction with the RebornOS logo
Typeface with which RebornOS is identified.

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/rebornos-branding/righteous-regular-font.git
```

Righteous Google Font:


https://fonts.google.com/specimen/Righteous?preview.text_type=custom
